package solomia;

import solomia.stateImpl.NewTaskState;

import java.util.LinkedList;
import java.util.List;

public class Kanban {
    private State state;
    private List<Task> taskList = new LinkedList<>();

    public Kanban() {
        state = new NewTaskState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void toDo() {
        state.toDo(this);
    }

    public void inProgress() {
        state.inProgress(this);
    }

    public void codeReview() {
        state.codeReview(this);
    }

    public void done() {
        state.done(this);
    }

    public void cancel() {
        state.cancel(this);
    }

    public void addTaskToList() {
        Task task = new Task();
        taskList.add(task);
        System.out.println(task + " added");
    }
}
