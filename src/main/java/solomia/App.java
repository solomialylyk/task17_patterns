package solomia;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Kanban kanban = new Kanban();
        do {
            System.out.println(" 1 - toDo");
            System.out.println(" 2 - inProgress" );
            System.out.println(" 3 - codeReview" );
            System.out.println(" 4 - done" );
            System.out.println(" 5 - cancel" );;
            System.out.println(" Q - exit");
            System.out.println(" Please, select menu point ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        kanban.toDo();
                        break;
                    case "2":
                        kanban.inProgress();
                        break;
                    case "3":
                        kanban.codeReview();
                        break;
                    case "4":
                        kanban.done();
                        break;
                    case "5":
                        kanban.cancel();
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
