package solomia.stateImpl;

import solomia.Kanban;
import solomia.State;

public class CancelState implements State {
    @Override
    public void cancel(Kanban kanban) {
        kanban.setState(new CancelState());
        System.out.println("Canceled");
    }

    @Override
    public void toDo(Kanban kanban) {
        kanban = new Kanban();
        kanban.addTaskToList();
    }
}
