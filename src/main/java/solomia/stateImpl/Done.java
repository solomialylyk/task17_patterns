package solomia.stateImpl;

import solomia.Kanban;
import solomia.State;

public class Done implements State {
    @Override
    public void done(Kanban kanban) {
        kanban.setState(new Done());
        System.out.println("Done!!!");
    }

    @Override
    public void toDo(Kanban kanban) {
        kanban.addTaskToList();
    }
}
