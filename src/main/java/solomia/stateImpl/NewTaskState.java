package solomia.stateImpl;

import solomia.Kanban;
import solomia.State;

public class NewTaskState implements State {
    @Override
    public void toDo(Kanban kanban) {
        kanban.addTaskToList();
    }

    @Override
    public void inProgress(Kanban kanban) {
        kanban.setState(new InProgress());
        System.out.println("In progress");
    }

    @Override
    public void cancel(Kanban kanban) {
        kanban.setState(new CancelState());
        System.out.println("Canceled");
    }
}
