package solomia.stateImpl;

import solomia.Kanban;
import solomia.State;

public class CodeReview implements State {
    @Override
    public void codeReview(Kanban kanban) {
        kanban.setState(new CodeReview());
        System.out.println("There is a code review");
    }

    @Override
    public void done(Kanban kanban) {
        kanban.setState(new Done());
        System.out.println("Done!");
    }

    @Override
    public void cancel(Kanban kanban) {
        kanban.setState(new CancelState());
        System.out.println("Canceled");
    }
}
