package solomia;

public class Task {
    private static int counter = 0;
    private int taskNumber = 0;
    private String name = "Task ";

    public Task() {
        counter++;
        taskNumber = counter;
    }

    @Override
    public String toString() {
        return "Task" + taskNumber;
    }
}
