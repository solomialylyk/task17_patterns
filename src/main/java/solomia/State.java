package solomia;

public interface State {
    default void toDo(Kanban kanban) {
        System.out.println("toDo is not allowed");
    }

    default void done(Kanban kanban) {
        System.out.println("Done is not allowed");
    }

    default void inProgress(Kanban kanban) {
        System.out.println("InProgress is not allowed");
    }

    default void codeReview(Kanban kanban) {
        System.out.println("CodeReview is not allowed");
    }
    default void cancel(Kanban kanban) {
        System.out.println("Cancel is not allowed");
    }
}
